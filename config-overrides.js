const devServerConfig = () => (config) => {
	return {
		...config,
		port               : 9005,
		index              : 'index.html',
		historyApiFallback : true,
		watchOptions       : { aggregateTimeout: 300, poll: 1000 },
		headers            : {
			'Access-Control-Allow-Methods' : 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
			'Access-Control-Allow-Headers' : 'X-Requested-With, content-type, Authorization',
			'Access-Control-Allow-Origin'  : '*'
		}
	};
};

module.exports = {
	
	devServer : overrideDevServer(devServerConfig())
};
